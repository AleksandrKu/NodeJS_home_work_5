const express = require('express');
const app = express();

const {Router} = require('express');
const router = Router();

const path = require('path');
const fileType = require('file-type');
const fs = require('fs');
const log_file = path.join(__dirname, "log.txt");

const events = require('events');
const emitter = new events.EventEmitter;

let start_time = '';
let start_dif_time = '';
let end_time = '';
let end_dif_time = '';

function function_time(time) {
    return time.getDate() + "." + (+time.getMonth() + 1) + "." + time.getFullYear() + "  " + time.getHours() + ":" + time.getMinutes() + ":" + time.getSeconds() + ":" + time.getMilliseconds();
}

emitter.on('start_time', () => {
    start_dif_time = new Date().getTime();
    start_time = function_time(new Date());
});

const not_found = function (res) {
    res.writeHead(404);
    res.end("Not found");
};

const write_stream_log = fs.createWriteStream(log_file, {flags: 'a'});

emitter.on('write_log', (data) => {
    let string = '';
    end_time = function_time(new Date());
    end_dif_time = new Date().getTime();
    string = "Request: " + start_time + " Response: " + end_time;
    string += " Time: " + (end_dif_time - start_dif_time) / 1000 + " seconds";
    string += data.text + " " + data.image;
    string += " statusCode: " + data.statusCode;
    string += " userAgent: " + data.userAgent;
    write_stream_log.write(string + "\n");
});

app.on('close', () => {
    write_stream.write("\n" + function_time(new Date()) + "Server was closed " + "\n");
    console.log("close");
});

router.get("/get-images", (req, res) => {
    fs.readdir(path.join(__dirname, 'images'), (err, files) => {
        console.log(err);
        console.log(files[1]);
        res.json({data: files});
    });
});

const database_file = path.join(__dirname, 'database.txt');
let users = [];
router.post("/user", (req, res) => {
    let obj = req.body;
    const write_stream = fs.createWriteStream(database_file, {flags: 'a'});
    users.push(obj);
    write_stream.write(function_time(new Date()) + " " + JSON.stringify(obj) + "\n");
    res.setHeader("Content-Type", "application/json");
    res.send(JSON.stringify({message: "Message added successful", _id: users.length, data: obj}));
});

router.get("/data", (req, res) => {
    let data = fs.readFileSync(database_file, 'utf8', (err) => {
        if (err) throw err;
    });
    console.log(users);
    users.forEach((item, i, arr) => {
        res.write(`<!DOCTYPE html>Name: <a href="/user/${i}"> ${item.userName} </a><br><br>`);
    });
    res.write("Info from database-file:<br>");
    res.write(data);
    res.end();
});


router.get('/user/:id', function (req, res) {
    if (users[req.params.id]) {
        res.setHeader("Content-Type", "text/html");
        res.write(JSON.stringify(users[req.params.id]));
    } else {
        res.write("Error");
    }
    res.end();
});


router.get('/images/:image', (req, res, next) => {
    const image_path = path.join(__dirname, "images", req.params.image);
    fs.stat(image_path, (err, stats) => {
        if (!(err == null && (stats["mode"] & 4))) { // file is readable
            const error = (err.code == "ENOENT") ? "No such file" : "Error";
            console.log(error);
            not_found(res);
            next(new Error('Error'));
        }
    });
    next();
}, function (req, res) {
    emitter.emit('start_time');
    const image_path = path.join(__dirname, "images", req.params.image);
    const read_stream = fs.createReadStream(image_path);
    read_stream.on('open', () => {
        read_stream.once('data', (chunk) => {
          let  file_type = fileType(chunk);
            if (file_type) {
                res.setHeader("Content-Type", file_type.mime);
            }
        });
        read_stream.pipe(res);
    });
    read_stream.on('end', () => {
        emitter.emit('write_log', {
            text: " Read image: ",
            image: req.params.image,
            statusCode: res.statusCode,
            userAgent: req.headers['user-agent']
        })
    });
});

router.get('/!*', function (req, res) {
    not_found(res);
    res.end();
});

module.exports = router;