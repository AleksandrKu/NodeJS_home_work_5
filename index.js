"use strict";
const express = require('express');
const app = express();
const routs = require('./routs');
const PORT = process.env.PORT || 9090;
const path = require('path');



app.use('/', express.static(path.join(__dirname, 'public')));

app.use(express.urlencoded());
app.use(express.json());
app.use(routs);

app.listen(PORT, () => {
    console.log("Server port: ", PORT);
});